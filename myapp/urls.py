from django.conf.urls import url
from django.contrib import admin

from myapp.views import get_saved_data, save_user_data, editdata,deletedata

urlpatterns = [
    url(r'^savedata', save_user_data ),
    url(r'^getsaveddata/', get_saved_data),
    url(r'^editdata', editdata),
    url(r'^deletedata', deletedata)
]
