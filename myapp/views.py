# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from myapp.models import Userinfo
# Create your views here.
from django.utils import timezone

@api_view(['POST'])
def save_user_data(request):
    if request.data:
        Userinfo(user=request.data['username'], usermsg=request.data['usermsg'], date=timezone.now()).save()
        data = {"message":"User information saved Successfully"}
    else:
        data = {"message":"Soory you are not sending any information"}
    return Response(data)


@api_view(['GET'])
def get_saved_data(request):
    ref = Userinfo.objects.all().values()
    return Response(ref)



@api_view(['POST'])
def editdata(request):
    print "reqqqqqq", request.data
    ref = Userinfo.objects.filter(id=request.data['id'])
    if ref:
        ref.update(user=request.data['user'], usermsg=request.data['usermsg'], date=timezone.now())
        data = {"message":"User information Updated Successfully"}
    else:
        data = {"message":"User information Not Available"}
    return Response(data)


@api_view(['POST'])
def deletedata(request):
    Userinfo.objects.filter(id=request.data['id'], usermsg=request.data['usermsg']).delete()
    data = {"message":"User information Deleted Successfully"}
    return Response(data)