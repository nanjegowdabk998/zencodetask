angular.module('myapp').service('datasaving', function($resource) {

    var url = $resource('http://127.0.0.1:8000/savedata');
    var urlget = $resource('http://127.0.0.1:8000/getsaveddata/');
    var urledit = $resource('http://127.0.0.1:8000/editdata');
    var urldelete = $resource('http://127.0.0.1:8000/deletedata');


    this.saveuserinfo = function(info) {
        return url.save(info).$promise;
    }

    this.getuserdetail = function() {
        return urlget.query().$promise;
    }
    this.editfun = function(data) {
        return urledit.save(data).$promise;
    }
    this.deletefun = function(data) {
        return urldelete.save(data).$promise;
    }

});
