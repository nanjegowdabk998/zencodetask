angular.module('myapp', ['ui.bootstrap','ui.router','ngAnimate']);

angular.module('myapp').config(function($stateProvider) {

    /* Add New States Above */

    $stateProvider.state('home',{
    	url:'/home',
    	templateUrl:'myapp/partial/datasaving/datasaving.html',
    	controller:'DatasavingCtrl'
    })
});

