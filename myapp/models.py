# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Userinfo(models.Model):
    user = models.CharField(max_length=100)
    usermsg = models.TextField()
    date = models.DateTimeField()

    def __str__(self):
        return self.user