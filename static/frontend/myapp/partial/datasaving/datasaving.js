angular.module('myapp').controller('DatasavingCtrl', function($state, datasaving, $scope, $uibModal) {

    $scope.adddatapop = function() {
        $scope.message = "hello"
        var mod = $uibModal.open({
            templateUrl: 'datafrompop',
            controller: 'popctrl',
            backdrop: 'static',
            keyboard: 'false',
            scope: $scope,
            size: 'md'
        })
    }

    $scope.editfunc = function(editdetails) {
        datasaving.editfun(editdetails).then(function(data) {
            $scope.response = data;
            var mod = $uibModal.open({
                templateUrl: 'dataresponse',
                controller: 'popctrl',
                backdrop: 'static',
                keyboard: 'false',
                scope: $scope,
                size: 'md'
            })
        })

    }
    
    $scope.deletefun = function(editdetails) {
        datasaving.deletefun(editdetails).then(function(data) {
            $scope.response = data;
            var mod = $uibModal.open({
                templateUrl: 'dataresponse',
                controller: 'popctrl',
                backdrop: 'static',
                keyboard: 'false',
                scope: $scope,
                size: 'md'
            })
        })

    }
    
    $scope.editpop = function(editdetails) {
        console.log("edddpop", editdetails)
        $scope.editdata = editdetails;
        var mod = $uibModal.open({
            templateUrl: 'editdata',
            controller: 'popctrl',
            backdrop: 'static',
            keyboard: 'false',
            scope: $scope,
            size: 'md'
        })

    }

    $scope.savedata = function(dataref) {
        datasaving.saveuserinfo(dataref).then(function(data) {
            $scope.response = data;
            var mod = $uibModal.open({
                templateUrl: 'dataresponse',
                controller: 'popctrl',
                backdrop: 'static',
                keyboard: 'false',
                scope: $scope,
                size: 'md'
            })
        })

    }

    function userobjget() {
        datasaving.getuserdetail().then(function(data) {
            if (data.length > 0) {
                $scope.checkdata = true;
            } else {
                $scope.checkdata = false;
            }
            $scope.userobj = data;
        })
    }
    userobjget()

    $scope.reloader = function() {
        $state.reload()
    }
});

angular.module('myapp').controller('popctrl', function($scope, $uibModalInstance) {

    $scope.closepop = function() {
        console.log("is is close function");
        $uibModalInstance.dismiss();

    }
});
